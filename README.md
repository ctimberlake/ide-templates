# CI Templates

The templates can be set at the instance level, the group level, or the project level.

For more informaiton, please see [Description Templates](https://docs.gitlab.com/ee/user/project/description_templates.html) documentation.
